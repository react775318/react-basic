import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ buttonText, onClick, ...props }) => (
	<button onClick={onClick} {...props}>
		{buttonText}
	</button>
);

Button.propTypes = {
	buttonText: PropTypes.string.isRequired,
	onClick: PropTypes.func,
};

Button.defaultProps = {
	onClick: () => {},
};

export default Button;
