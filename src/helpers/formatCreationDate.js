const formatDate = (creationDate) => {
	const [month, day, year] = creationDate.split('/');
	return `${day}.${month}.${year}`;
};

export default formatDate;
