import React, { useState } from 'react';
import Courses from './components/Courses/Courses';
import Header from './components/Header/Header';
import CourseInfo from './components/CourseInfo/CourseInfo';
import { mockedCoursesList, mockedAuthorsList } from './constants';
import './App.css';

function App() {
	const [selectedCourse, setSelectedCourse] = useState(null);

	const authorsMap = mockedAuthorsList.reduce((acc, author) => {
		acc[author.id] = author.name;
		return acc;
	}, {});

	const showCourse = (course) => {
		setSelectedCourse(course);
	};

	const goBack = () => {
		setSelectedCourse(null);
	};

	return (
		<div className='app'>
			<Header />
			{selectedCourse ? (
				<CourseInfo
					course={selectedCourse}
					authorsMap={authorsMap}
					onBack={goBack}
				/>
			) : (
				<Courses
					courses={mockedCoursesList}
					authorsMap={authorsMap}
					onShowCourse={showCourse}
				/>
			)}
		</div>
	);
}

export default App;
