import React from 'react';
import './EmptyCourseList.css';
import Button from '../../common/Button/Button';

const EmptyCourseList = () => (
	<div className='empty-course-list'>
		<h2>Course List is Empty</h2>
		<p>Please use "Add New Course" button to add your first course.</p>
		<Button buttonText='Add new course' />
	</div>
);

export default EmptyCourseList;
