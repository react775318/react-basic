import Button from '../../common/Button/Button';
import styles from './Header.module.css';
import Logo from './components/Logo/Logo';

const Header = (props) => {
	const name = 'Salimbek Kairbekov';
	const isAuthentificated = true;

	return (
		<header className={styles.header}>
			<Logo />
			<span>{name}</span>
			<Button buttonText={isAuthentificated ? 'Logout' : 'Login'} />
		</header>
	);
};

export default Header;
