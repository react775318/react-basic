import logo from '../../../../assets/logo.png';

const Logo = () => {
	return <img src={logo} alt='logo' width={100} />;
};

export default Logo;
