import React from 'react';
import PropTypes from 'prop-types';
import CourseCard from './components/CourseCard/CourseCard';
import EmptyCourseList from '../EmptyCourseList/EmptyCourseList';
import './Courses.css';
import Button from '../../common/Button/Button';

const Courses = ({ courses, authorsMap, onShowCourse }) => (
	<div className='courses'>
		<div className='search-bar'>
			{/* Something long for searchbar component */}
		</div>
		{courses.length > 0 ? (
			<div className='course-list'>
				<Button buttonText='Add New Course' />
				{courses.map((course) => (
					<CourseCard
						key={course.id}
						course={course}
						authorsMap={authorsMap}
						onShowCourse={onShowCourse}
					/>
				))}
			</div>
		) : (
			<EmptyCourseList />
		)}
	</div>
);

Courses.propTypes = {
	courses: PropTypes.array.isRequired,
	authorsMap: PropTypes.object.isRequired,
	onShowCourse: PropTypes.func.isRequired,
};

export default Courses;
