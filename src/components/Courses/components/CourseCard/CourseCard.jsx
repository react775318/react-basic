import React from 'react';
import PropTypes from 'prop-types';
import './CourseCard.css';
import formatDuration from '../../../../helpers/getCourseDuration';
import formatDate from '../../../../helpers/formatCreationDate';

const CourseCard = ({ course, authorsMap, onShowCourse }) => {
	const { title, description, creationDate, duration, authors } = course;

	const formattedDuration = formatDuration(duration);

	const formattedDate = formatDate(creationDate);

	// Get authors' names
	const authorsNames = authors
		.map((authorId) => authorsMap[authorId])
		.join(', ');

	return (
		<div className='course-card'>
			<h2>{title}</h2>
			<p>{description}</p>
			<div className='course-info'>
				<p>
					<strong>Authors:</strong>{' '}
					{authorsNames.length > 50
						? authorsNames.substring(0, 47) + '...'
						: authorsNames}
				</p>
				<p>
					<strong>Duration:</strong> {formattedDuration}
				</p>
				<p>
					<strong>Created:</strong> {formattedDate}
				</p>
			</div>
			<button onClick={() => onShowCourse(course)}>Show course</button>
		</div>
	);
};

CourseCard.propTypes = {
	course: PropTypes.object.isRequired,
	authorsMap: PropTypes.object.isRequired,
	onShowCourse: PropTypes.func.isRequired,
};

export default CourseCard;
