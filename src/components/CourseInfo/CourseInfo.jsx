import React from 'react';
import PropTypes from 'prop-types';
import formatDuration from '../../helpers/getCourseDuration';
import formatDate from '../../helpers/formatCreationDate';
import './CourseInfo.css';
import Button from '../../common/Button/Button';

const CourseInfo = ({ course, authorsMap, onBack }) => {
	const { id, title, description, creationDate, duration, authors } = course;

	const formattedDuration = formatDuration(duration);

	const formattedDate = formatDate(creationDate);

	// Get authors' names
	const authorsNames = authors
		.map((authorId) => authorsMap[authorId])
		.join(', ');

	return (
		<div className='course-info'>
			<h2>{title}</h2>
			<p>{description}</p>
			<p>
				<strong>ID:</strong> {id}
			</p>
			<div className='course-info-details'>
				<p>
					<strong>Duration:</strong> {formattedDuration}
				</p>
				<p>
					<strong>Created:</strong> {formattedDate}
				</p>
				<p>
					<strong>Authors:</strong> {authorsNames}
				</p>
			</div>
			<Button buttonText='Back to the courses' onClick={onBack} />
		</div>
	);
};

CourseInfo.propTypes = {
	course: PropTypes.object.isRequired,
	authorsMap: PropTypes.object.isRequired,
	onBack: PropTypes.func.isRequired,
};

export default CourseInfo;
