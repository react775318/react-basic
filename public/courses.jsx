import React, { useState } from 'react';
import { mockedAuthorsList, mockedCoursesList } from '../../constants';
import CourseCard from './components/CourseCard/CourseCard';
import CourseInfo from '../CourseInfo/CourseInfo';

const Courses = () => {
	const [selectedCourse, setSelectedCourse] = useState(null);

	const authorsMap = mockedAuthorsList.reduce((acc, author) => {
		acc[author.id] = author.name;
		return acc;
	}, {});

	const showCourse = (course) => {
		setSelectedCourse(course);
	};

	return (
		<div>
			{selectedCourse ? (
				<CourseInfo course={selectedCourse} authorsMap={authorsMap} />
			) : (
				<div className='courses'>
					{mockedCoursesList.map((course) => (
						<CourseCard
							key={course.id}
							course={course}
							authorsMap={authorsMap}
							onShowCourse={showCourse}
						/>
					))}
				</div>
			)}
		</div>
	);
};

export default Courses;
